function obterVagas(id){
    $.ajax({
    url : '/vagaCadastrada/' + id,
        type : 'POST',
        success: function(res){             
        var vagaObj = JSON.parse(res);
        $('#'.concat(id)).empty();
        $('#listTemplate').tmpl(vagaObj).appendTo('#'.concat(id));      
        },
        error: function(error){
            console.log(error);
        }
        });
}

function listarCandidatos(id){
  $.ajax({
  url : '/candidatos/' + $(id).attr('data-id'),
      type : 'POST',
      success: function(res){             
      var vagaObj = JSON.parse(res);
      $('#todosOsCandidatos').empty();
      if (vagaObj.length == 0){
        $('#todosOsCandidatos').append('<h3 class="text-center">Nenhum usuário se candidatou nessa vaga</h3>')
      }else{
        $('#listCandidatos').tmpl(vagaObj).appendTo('#todosOsCandidatos'); 
      } 
      $('#listarCandidatos').modal('show');    
      },
      error: function(error){
          console.log(error);
      }
      });
}
function verDetalhes(id){
  $.ajax({
  url : '/vagaCadastrada/' + $(id).attr('data-id'),
      type : 'POST',
      success: function(res){             
      var detalhes = JSON.parse(res);
      $('#conteudoDetalhes').text(detalhes[0]['descricao']);  
      $('#detalhes').modal('show');    
      },
      error: function(error){
          console.log(error);
      }
      });
}

function editarVaga(id) { 
    $('#editar').modal('show');
    localStorage.setItem('editId', $(id).attr('data-id'));
    $.ajax({
        url: '/vagaCadastrada/'+ $(id).attr('data-id'),
        data: {
            id: $(id).attr('data-id')
        },
        type: 'POST',
        success: function(res) {
          var data = JSON.parse(res);

            $('#titulo').val(data[0]['titulo']);
            $('#descricao').val(data[0]['descricao']);
            $('#curso').val(data[0]['curso']);
            prazo = data[0]['prazoFinal']
            var dia  = prazo.split("/")[0];
            var mes  = prazo.split("/")[1];
            var ano  = prazo.split("/")[2];
            var dataFormatada = ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
            $('#prazoFinal').val(dataFormatada);

            
        },
        error: function(error) {
            console.log(error);
        }
    });
}
$(function(){
$(".linkFavoritar").click(function( event ) {
  event.preventDefault();
  var id = this.id;   // Getting Button id
  var split_id = id.split("_");
  var text = split_id[0];
  var vaga_id1 = split_id[1]
  $.ajax({
    url:`/fav/${vaga_id1}/${text}`,
    type: 'POST',
    data: {vaga_id: vaga_id1, favoudesfav: text },
    dataType: 'json',
    success: () => {
      if (text == 'fav') {
        $('#'.concat('fav_'.concat(vaga_id1))).text('').append('<i class="fas fa-star botaoFavorito"></i>');
        $('#'.concat('fav_'.concat(vaga_id1))).attr("id", "desfav_".concat(vaga_id1));
      }
      else {
        $('#'.concat('desfav_'.concat(vaga_id1))).text('').append('<i class="far fa-star botaoFavorito2"></i>');
        $('#'.concat('desfav_'.concat(vaga_id1))).attr("id", "fav_".concat(vaga_id1));
      }
    },
      error: function(error) {
        console.log(error);
    }
  });
})
})

$(function(){
  $(".linkCandidatar").click(function( event ) {
    event.preventDefault();
    var id = this.id;   // Getting Button id
    var split_id = id.split("_");
    var text = split_id[0];
    var vaga_id1 = split_id[1]
    $.ajax({
      url:`/cand/${vaga_id1}/${text}`,
      type: 'POST',
      data: {vaga_id: vaga_id1, favoudesfav: text },
      dataType: 'json',
      success: () => {
        if (text == 'cand') {
          $('#'.concat('cand_'.concat(vaga_id1))).removeClass('btn-primary').addClass('btn-secondary').text("Remover Inscrição");
          $('#'.concat('cand_'.concat(vaga_id1))).attr("id", "descand_".concat(vaga_id1));
        }
        else {
          $('#'.concat('descand_'.concat(vaga_id1))).removeClass('btn-secondary').addClass('btn-primary').text("Candidatar-se");
          $('#'.concat('descand_'.concat(vaga_id1))).attr("id", "cand_".concat(vaga_id1));
        }
      },
        error: function(error) {
          console.log(error);
      }
    });
  })
  })
function confirmarExclusao(id){
  $('#deletar').modal('show');
    localStorage.setItem('deleteId',$(id).attr('data-id'));
}

function deletar(){
    $.ajax({
    url : '/apagarVagas',
    data : {id:localStorage.getItem('deleteId')},
    type : 'POST',
    success: function(res){
      var result = JSON.parse(res);
      if(result.status == 'OK'){
        $('#deletar').modal('hide');
        document.getElementById(localStorage.getItem('deleteId')).remove();
      }
      else{
        alert(result.status); 
      }
    },
    error: function(error){
      console.log(error);
    }
  });
}

$(function(){   
  $('#salvar').click(function(event){
    event.preventDefault();
    $.ajax({
      url : '/atualizarVagas',
      dataType: 'json',
      data : { titulo:$('#titulo').val(),
               descricao:$('#descricao').val(),
               curso:$('#curso').val(),
               prazoFinal:$('#prazoFinal').val(),
               id:localStorage.getItem('editId')},
      type : 'POST',
      success: function(res){       

        $('#editar').modal('hide');               
        obterVagas(localStorage.getItem('editId'));         
      },
      error: function(error){
        console.log(error);
      }
    });
    })
});

$(function(){
  var $input    = document.getElementById('picture'),
      $fileName = document.getElementById('foto-name');

  $input.addEventListener('change', function(){
  $fileName.textContent = this.value;
});
})

$(function(){
  var $input    = document.getElementById('curriculo'),
      $fileName = document.getElementById('curriculo-name');

  $input.addEventListener('change', function(){
  $fileName.textContent = this.value;
});
})

function editarEgresso(id) { 
  $('#editar').modal('show');
  localStorage.setItem('editId', $(id).attr('data-id'));
  $.ajax({
      url: '/egresso/'+ $(id).attr('data-id'),
      data: {
          id: $(id).attr('data-id')
      },
      type: 'POST',
      success: function(res) {
        var data = JSON.parse(res);

          $('#username').val(data[0]['username']);

          
      },
      error: function(error) {
          console.log(error);
      }
  });
}

$(function(){   
  $('#salvarEgresso').click(function(event){
    event.preventDefault();
    $.ajax({
      url : '/atualizarEgresso',
      dataType: 'json',
      data : { username:$('#username').val(),
               id:localStorage.getItem('editId')},
      type : 'POST',
      success: function(res){

        var result = JSON.parse(JSON.stringify(res));
        if(result.status == 'OK'){
          $('#editar').modal('hide');
          $('#span').text('')
          obterEgressos(localStorage.getItem('editId'))
        }
        else if(result.status == 'VAZIO'){
          $('#span').text('Campo obrigatório')
        }  
        else{
          $('#span').text('Nome de Usuário já existente')
        }  
      },
      error: function(error){
        console.log(error);
      }
    });
    })
});

function obterEgressos(id){
  $.ajax({
  url : '/egresso/' + id,
      type : 'POST',
      success: function(res){             
      var vagaObj = JSON.parse(res);
      $('#'.concat(id)).empty();
      $('#listTemplate').tmpl(vagaObj).appendTo('#'.concat(id));      
      },
      error: function(error){
          console.log(error);
      }
      });
}

function apagarEgresso(id){
  $('#deletar').modal('show');
    localStorage.setItem('deleteId',$(id).attr('data-id'));
}


function deletarEgresso(){
  $.ajax({
  url : '/apagarEgresso',
  data : {id:localStorage.getItem('deleteId')},
  type : 'POST',
  success: function(res){
    var result = JSON.parse(res);
    if(result.status == 'OK'){
      $('#deletar').modal('hide');
      document.getElementById(localStorage.getItem('deleteId')).remove();
    }
    else{
      alert(result.status); 
    }
  },
  error: function(error){
    console.log(error);
  }
});
}