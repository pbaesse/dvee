import os
import secrets
import flask_excel as excel
from PIL import Image
from flask import render_template, flash, redirect, url_for, request, abort, jsonify, json, send_from_directory
from app import app, db
from app.forms import LoginForm, RegistrationForm, VagaForm, PerfilForm, ImportarForm
from flask_login import current_user, login_user, logout_user, login_required
from app.models import User, Vagas, Fav_vaga, Insc_vaga
from werkzeug.urls import url_parse
from datetime import datetime, date

excel.init_excel(app)

def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/img', picture_fn)
    form_picture.save(picture_path)

    return picture_fn

def save_file(form_file):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_file.filename)
    file_fn = random_hex + f_ext
    file_path = os.path.join(app.root_path, 'static/arquivos', file_fn)
    form_file.save(file_path)
    
    return file_fn

@app.errorhandler(405)
def error_405(error):
    info = "Não é possível acessar essa página"
    return render_template('erros.html', title="Erro", info=info)

@app.errorhandler(404)
def error_404(error):
    info = "O endereço solicitado não foi encontrado"
    return render_template('erros.html', title="Página não encontrada", info=info)

@app.errorhandler(403)
def error_403(error):
    info = "Você não tem permissão para acessar essa página"
    return render_template('erros.html', title="Acesso negado", info=info)

@app.route('/', methods=['GET', 'POST'])
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(matricula_cnpj=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Nome de usuário ou senha invalidos!', 'danger')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        next_page = request.args.get('next')
        if not next_page or url_parse(next_page).netloc != '':
            next_page = url_for('index')
        return redirect(next_page)
    return render_template('login.html', title='Entrar', form=form)

@app.route('/index')
@login_required
def index():
    return render_template("index.html", title='Página inicial')

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data, type_user='empr', matricula_cnpj=form.matricula_cnpj.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Cadastro realizado com sucesso!', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Cadastre-se', form=form)

@app.route('/novaVaga', methods=['GET', 'POST'])
@login_required
def novaVaga():
    if current_user.type_user != 'empr':
        abort(403)
    form = VagaForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            image = picture_file
        else:
            image = "vaga.png"
        vagas = Vagas(titulo=form.titulo.data, curso=form.curso.data, descricao=form.descricao.data, prazoFinal=form.prazoFinal.data, autor=current_user, foto_vaga=image)
        db.session.add(vagas)
        db.session.commit()
        flash('Vaga cadastrada com sucesso!', 'success')
        return redirect(url_for('suasVagas'))
    return render_template('novaVaga.html', title='Cadastrar uma vaga', form=form)

@app.route('/suasVagas', methods=['GET', 'POST'])
@login_required
def suasVagas():
    form = VagaForm()
    page = request.args.get('page', 1, type=int)
    if current_user.type_user != 'empr':
        abort(403)
    vaga = Vagas.query.filter_by(autor=current_user).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    return render_template('suasVagas.html', title='Suas vagas', vaga=vaga, form=form)
    
@app.route('/vagaCadastrada/<int:idVaga>', methods=['POST'])
@login_required
def vagaCadastrada(idVaga):
    vagas = Vagas.query.get_or_404(idVaga)
    vagas_dict = []
    vaga_dict = {
        'id': vagas.id,
        'titulo': vagas.titulo,
        'curso': vagas.curso,
        'descricao': vagas.descricao,
        'prazoInicial': vagas.prazoInicial.strftime('%d/%m/%Y'),
        'prazoFinal': vagas.prazoFinal.strftime('%d/%m/%Y'),
        'autor': vagas.autor.username,
        'imagem':vagas.foto_vaga}
    vagas_dict.append(vaga_dict)
    return json.dumps(vagas_dict)

@app.route('/atualizarVagas', methods=['POST'])
@login_required
def atualizarVagas():
    _id = request.form['id']
    vaga = Vagas.query.get_or_404(_id)
    vaga.titulo = request.form['titulo']
    vaga.curso = request.form['curso']
    vaga.descricao = request.form['descricao']
    data = date.fromisoformat(request.form['prazoFinal'])
    vaga.prazoFinal = data
    db.session.commit()
    return json.dumps({'status':'OK'})
    
@app.route('/apagarVagas', methods=['POST'])
@login_required
def apagarVagas():
    _id = request.form['id']
    vaga = Vagas.query.get_or_404(_id)
    db.session.delete(vaga)
    db.session.commit()
    return json.dumps({'status':'OK'})

@app.route('/perfil', methods=['GET','POST'])
@login_required
def perfil():
    form = PerfilForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.foto_perfil = picture_file
        if form.curriculo.data:
            curriculo_file = save_file(form.curriculo.data)
            current_user.curriculo = curriculo_file
        current_user.username = form.username.data
        current_user.email = form.email.data
        current_user.telefone = form.telefone.data
        current_user.info = form.info.data
        db.session.commit()
        flash('Perfil atualizado com sucesso', 'success')
        return redirect(url_for('perfil'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.email.data = current_user.email
        form.telefone.data = current_user.telefone
        form.info.data = current_user.info
    image_file = url_for('static', filename='img/' + current_user.foto_perfil)
    curriculo_file = url_for('static', filename='arquivos/' + current_user.curriculo)
    return render_template('perfil.html', title='Perfil', foto=image_file, form=form, curriculo=curriculo_file)

@app.route('/vagas')
@login_required
def vagas():
    if current_user.type_user != 'egr':
        abort(403)
    data_agora = datetime.now()
    page = request.args.get('page', 1, type=int)
    vagas = Vagas.query.filter(Vagas.prazoFinal > data_agora).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    return render_template('vagas.html', title='Vagas', vaga=vagas, data_agora= data_agora)

@app.route('/vagas/<action>/<string:elemento>')
@login_required
def vagasfiltro(action, elemento):
    if current_user.type_user != 'egr':
        abort(403)
    data_agora = datetime.now()
    page = request.args.get('page', 1, type=int)
    if action == 'curso':
        vagas = Vagas.query.filter_by(curso=elemento).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    elif action == 'status':
        if elemento == 'todos':
            vagas = Vagas.query.order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
        elif elemento == 'andamento':
            vagas = Vagas.query.filter(Vagas.prazoFinal > data_agora).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
        elif elemento =='encerrado':
            vagas = Vagas.query.filter( data_agora > Vagas.prazoFinal).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    return render_template('vagas.html', title='Vagas', vaga=vagas, data_agora=data_agora)

@app.route('/vagas/favoritos')
@login_required
def vagasfavoritas():
    if current_user.type_user != 'egr':
        abort(403)
    data_agora = datetime.now()
    page = request.args.get('page', 1, type=int)
    vagas = Vagas.query.join(Fav_vaga, (Fav_vaga.vaga_id == Vagas.id)).filter(Fav_vaga.user_id == current_user.id).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    return render_template('vagas.html', title='Vagas', vaga=vagas, data_agora=data_agora)

@app.route("/search", methods=["GET", "POST"])
@login_required
def search():
    q = request.args.get('q')
    data_agora = datetime.now()
    page = request.args.get('page', 1, type=int)
    if q != "":
        vagas = Vagas.query.filter(Vagas.titulo.contains(q) | Vagas.descricao.contains(q)).order_by(Vagas.prazoInicial).paginate(page=page, per_page=4)
        return render_template('vagas.html', title="Pesquisar " + q, user=user, vaga=vagas, data_agora=data_agora)
    else:
        return redirect(request.referrer) 

@app.route("/usuario/<string:username>")
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    if current_user == user:
        return redirect(url_for("perfil"))
    else:
        image_file = url_for('static', filename='img/' + user.foto_perfil)
        return render_template('usuario.html', title=user.username, user=user, foto=image_file)

@app.route("/fav/<int:vaga_id>/<action>", methods=['POST'])
@login_required
def favoritar_vaga(vaga_id, action):
    vaga = Vagas.query.get_or_404(vaga_id)
    favoudesfav = action
    if favoudesfav == 'fav':
        current_user.favoritar_vaga(vaga)
        db.session.commit()
    if favoudesfav == 'desfav':
        current_user.desfavoritar_vaga(vaga)
        db.session.commit()
    return json.dumps({'status':'OK'})

@app.route("/cand/<int:vaga_id>/<action>", methods=['POST'])
@login_required
def candidatar_vaga(vaga_id, action):
    vaga = Vagas.query.get_or_404(vaga_id)
    candoudescand = action
    if candoudescand == 'cand':
        current_user.candidatar_vaga(vaga)
        db.session.commit()
    if candoudescand == 'descand':
        current_user.descandidatar_vaga(vaga)
        db.session.commit()
    return json.dumps({'status':'OK'})

@app.route("/candidatos/<int:cand_id>", methods=['POST'])
@login_required
def candidatos(cand_id):
    inscritos = User.query.join(Insc_vaga, (Insc_vaga.user_id == User.id)).filter(Insc_vaga.vaga_id == cand_id).order_by(User.username)
    inscritos_dict = []
    for inscrito in inscritos:
        inscrito_dict = {
            'id': inscrito.id,
            'username': inscrito.username,
            'foto': inscrito.foto_perfil}
        inscritos_dict.append(inscrito_dict)
    return json.dumps(inscritos_dict)

@app.route("/inscricoes")
@login_required
def inscricoes():
    if current_user.type_user != 'egr':
        abort(403)
    data_agora = datetime.now()
    page = request.args.get('page', 1, type=int)
    vagas = Vagas.query.join(Insc_vaga, (Insc_vaga.vaga_id == Vagas.id)).filter(Insc_vaga.user_id == current_user.id).order_by(Vagas.prazoInicial.desc()).paginate(page=page, per_page=4)
    return render_template('inscricoes.html', title="Inscrições", user=user, vaga=vagas, data_agora=data_agora)

@app.route("/egressos")
@login_required
def egressos():
    page = request.args.get('page', 1, type=int)
    form = PerfilForm()
    if current_user.type_user != 'adm':
        abort(403)
    user = User.query.filter(User.type_user =='egr').order_by(User.username).paginate(page=page, per_page=10)
    return render_template ('usuariosCadastrados.html', title="Egressos", user=user, form=form)

@app.route("/empresas")
@login_required
def empresas():
    if current_user.type_user != 'adm':
        abort(403)
    page = request.args.get('page', 1, type=int)
    user = User.query.filter(User.type_user =='empr').order_by(User.username).paginate(page=page, per_page=10)
    return render_template ('usuariosCadastrados.html', title="Empresas", user=user)

@app.route("/cadastrarEgressos", methods=['GET', 'POST'])
@login_required
def cadastrarEgressos():
    if current_user.type_user != 'adm':
        abort(403)
    form = ImportarForm()
    if request.method == 'POST':
        if form.validate_on_submit():
            def user_init_func(row):
                u = User(username=row['Nome'], matricula_cnpj=row['Matrícula'], email=row['E-mail Pessoal'], type_user='egr' )
                u.set_password(row['Matrícula'])
                return u
            request.save_to_database(
                field_name='arquivo', session=db.session,
                table=User,
                initializer=user_init_func)
            flash('Cadastro realizado com sucesso!', 'success')
            return redirect(url_for('egressos'), code=302)
    return render_template ('cadastrarEgressos.html', title="Cadastrar Egressos", form=form)

@app.route('/egresso/<int:idEgresso>', methods=['POST'])
@login_required
def egresso(idEgresso):
    egressos = User.query.get_or_404(idEgresso)
    egressos_dict = []
    egresso_dict = {
        'id': egressos.id,
        'username': egressos.username,
        'imagem': egressos.foto_perfil}
    egressos_dict.append(egresso_dict)
    return json.dumps(egressos_dict)

@app.route('/atualizarEgresso', methods=['POST'])
@login_required
def atualizarEgresso():
    _id = request.form['id']
    egresso = User.query.get_or_404(_id)
    if request.form['username'] == '':
        return json.dumps({'status':'VAZIO'})
    else:
        usuarios = User.query.filter_by(username = request.form['username'])
        if usuarios.count() > 0:
            if usuarios.count() == 1:
                for usuario in usuarios:
                    userId = usuario.id
                if str(userId) == str(_id):
                    egresso.username = request.form['username']
                    db.session.commit()
                    return json.dumps({'status':'OK'})
                else:
                    return json.dumps({'status':'FAIL'})
        else: 
            egresso.username = request.form['username']
            db.session.commit()
            return json.dumps({'status':'OK'})

@app.route('/apagarEgresso', methods=['POST'])
@login_required
def apagarEgresso():
    _id = request.form['id']
    user = User.query.get_or_404(_id)
    db.session.delete(user)
    db.session.commit()
    return json.dumps({'status':'OK'})

@app.route('/sobre')
@login_required
def sobre():
    return render_template ('sobre.html', title="O que é o DVEE")